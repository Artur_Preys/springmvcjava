package controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import beans.Book;
import beans.BooksDB;
import beans.LogEntry;
import beans.User;

@Controller
public class HelloController {

    @Autowired
    BooksDB booksDB;

    @RequestMapping("/")
    public String display() {
        return "index";
    }

    @RequestMapping("/hello")
    public String hello() {
        return "index";
    }

    @RequestMapping("/logs")
    public String logs(Model model) {
        LogEntry logEntry = new LogEntry("debug", "mvc working fine");
        LogEntry logEntry2 = new LogEntry("error", "critical error");
        List<LogEntry> entries = Arrays.asList(logEntry, logEntry2);
        model.addAttribute("testLogs", entries);
        return "logView";
    }
    @RequestMapping("/books")
    public String books(Model model) {
        model.addAttribute("books", booksDB.getBooks());
        return "booksView";
    }




    @RequestMapping("/addUser")
    public String addUser(Model model) {

        model.addAttribute("user", new User());
        return "addUser";
    }

    @RequestMapping("/addBook")
    @Secured(value="ROLE_ADMIN")
    public String addBook(Model model, @ModelAttribute Book book) {
        if(book != null && book.getName() != null){
            booksDB.addBook(book);
        }
        else {
            model.addAttribute("book", new Book());
        }
        model.addAttribute("allBooks", booksDB.getBooks());

        return "addBook";
    }



    @RequestMapping("/redirect")
    public String redirect(@ModelAttribute User user) {
        return "final";
    }










}
